import requests
import csv
from bs4 import BeautifulSoup as bs
import urllib
import os

def scrape_and_run(genre):

    page = requests.get("https://www.goodreads.com/list/show/1.Best_Books_Ever?page=1")
    soup = bs(page.content, 'html.parser')
    titles = soup.find_all('a', class_='bookTitle')
    authors = soup.find_all('a', class_='authorName')
    images = soup.find_all('a', class_='leftAlignedImage')

    books_save = 0

    for title, author in zip(titles, authors):
        i = 0

        try:
            ## single book page
            book_page = requests.get("https://www.goodreads.com" + title['href'])
            soup = bs(book_page.content, 'html.parser')
            title_name = title.get_text()

            print(images[i].img["src"],title_name, author.get_text())
            books_save += 1

        except OSError as exc:
            if exc.errno == 36:
                print(exc)
        i += 1

    print("%d %s books saved." % (books_save, genre)) # books count feedback



if __name__ == '__main__':

    ## run ifinite till user tells you to stop
    ## to avoid having to compile again an d again
    while True:
        genre = input("Enter the genre (or quit to stop): ").lower() # input case lowered
        # genre = "Fiction"
        if(genre == "quit"):
            break
        else:
            scrape_and_run(genre)


